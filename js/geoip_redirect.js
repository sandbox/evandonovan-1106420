$(document).ready(function() {
  /* Hides the label.
	Adjust for what you need: #FORM-ID to identify the form. 
	This way it doesn't hit other forms unintentionally. */
  $("#geoip-redirect-search-form").find("label").hide();

 // Defines variable for label.
 var zipcode = 'Zipcode';
  // Moves zipcode label into box
  if (($('#edit-zipcode').val() == '' ) || ($('#edit-zipcode').val() == zipcode)) {
    $('#edit-zipcode').attr('value', zipcode);
		$('#edit-zipcode').css('color', '#4F4F4F');
  }
	// Hides label when it gains focus
  $('#edit-zipcode').focus(function() {
    if ($(this).val() == zipcode) {
      $(this).attr('value', '');
			$(this).css('color', '#000000');
    }
  });
	// Shows label when it loses focus
  $('#edit-zipcode').blur(function() {
    if ($(this).val() == '') {
      $(this).attr('value', zipcode);
			$(this).css('color', '#4F4F4F');
    }
  });
	
	// Controls zipcode box visibility based on changing country value
	$('#edit-country').bind('change keyup', function() {
		// US has postal codes for which we have data, so show box																							 
	  if ($(this).val() == 'us') {
			  $('#edit-zipcode').attr('value', zipcode);
			  $('#edit-zipcode').show();				
		}
		// Hide box for other countries
		else {
			  $('#edit-zipcode').hide();
				$('#edit-zipcode').attr('value', '');
		}
	});

});   